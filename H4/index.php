<?php
//@ autor Danylo Podshybiakin <dan.podshibiakin29@gmail.com>

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h3>--------------------создать форму оставить отзыв--------------------</h3>
<form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method='post'>
    <h4>Your name:</h4>
    <input type="text" name="userName" value="" required>
    <h4>Leave your response here:</h4>
    <textarea name='text' cols='80' rows="10">
    </textarea>
    <input type='submit' name='sendIt' value='Send a response!'>
</form>
<span><?php
    if (@$_REQUEST['reviewSubmit']) {
    echo '<pre>';
    print_r($_REQUEST);
    echo '</pre>';
}
?>
</span>
<h3>--------------------создать форму добавить товар в корзину--------------------</h3>
<form action="<?php $_SERVER['SCRIPT_NAME']; ?>" method="post">
    <h4>Choose a product below:</h4>
    <select name="productType" id="product_type">
        <option value="Apple">Apple</option>
        <option value="Chocolate">Chocolate</option>
        <option value="Soda">Soda</option>
        <option value="Bread">Bread</option>
        <option value="Mango">Mango</option>
        <option value="Salt">Salt</option>
    </select>
<h4>Amount of items</h4>
    <input type="number" name="productAmount" min="1" placeholder="put number here">
    <input type="submit" name="addToBag" value="Add to bag">
</form>
<?php
session_start();
$_SESSION['productType'] = $_REQUEST['productType'];
$_SESSION['productAmount'] = $_REQUEST['productAmount'];

echo $_SESSION['productType'];
echo '<br>';
echo $_SESSION['productAmount'];

if (@$_REQUEST['addToCard']) {
    echo '<pre>';
    print_r($_REQUEST);
    echo '</pre>';
}
?>
<h3>--------------------обратная связь перезвонить клиенту--------------------</h3>
<form action="<?php $_SERVER['SCRIPT_NAME']; ?>" method="post">
    <h4>We will call you back</h4>
    <h4>Your Name:</h4>
    <input type="text" name="name" placeholder="Name">
    <input type="tel" name="phone" placeholder="Your phone">
    <input type="submit" name="submitResponse" value="Give a callback">
</form>
<?php
if (@$_REQUEST['submitResponse']) {
echo '<pre>';
    print_r($_REQUEST);
    echo '</pre>';
}
?>
<h3>--------------------создать форму оформить заказ--------------------</h3>
<form action="<?php $_SERVER['SCRIPT_NAME']; ?>" method='post'>
<h4>Your Name</h4>
    <input type="text" name="name" placeholder="Your Name">
    <h4>Your phone</h4>
    <input type="tel" name="phoneNum" placeholder="Your Phone">
    <h4>Choose delivery type</h4>
    <select name="deliveryType">
        <option>NovaPoshta</option>
        <option>self-pickup</option>
        <option>MeestExpres</option>
        <option>Glovo</option>
    </select>
    <h4>Your Address</h4>
    <input type="text" name="userAddress" placeholder="Write your address" >
    <input type="submit" name="send" value="Send">

    <?php
    if (!empty($_POST)) {
        $_SESSION['user'] = $_POST;
    }
?>
</body>
</html>