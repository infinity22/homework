<?php
//@ autor Danylo Podshybiakin <dan.podshibiakin29@gmail.com>
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h2>Homework</h2>
<h3>First part "*"</h3>
<h4>Task 1</h4>
<p>Dано два числа 42 и 55 определите при помощи тернарной операции какое число больше.</p>
<ul>
    <li><?php
        $firstNum = 42; $secondNum = 55;
        echo $secondNum > $firstNum ? 'It`s more then 42' : 'It`s not more than 42'?>
    </li>
</ul>
<h4>Task 2</h4>
<p>Используй вместо статических чисел функцию rand() для задачи (1); </p>
<ul>
    <li><?php
        $fo = rand(5, 15);
        $bo = rand(5, 15);

        echo rand() . "\n";
        echo "<br>";
        echo rand() . "\n";
        echo "<br>";
        echo rand(5, 15);
        ?>
    </li>
</ul>
<h4>Task 3</h4>
<p>Сокращение Имени и Отчества.</p>
<ul>
    <li><?php
        $familyName = 'Podshybiakin';
        $firstName = 'Danylo';
        $patronicName = 'V`yacheslavovich';

        echo $familyName . ' ' . $firstName[0] . '. ' . $patronicName[0] . '.'
        ?>
    </li>
</ul>
<h4>Task 4</h4>
<p>Нужно разработать программу, которая считала бы количество вхождений какой-нибудь выбранной вами цифры в выбранном вами числе. </p>
<ul>
    <li><?php
        $whatNum = rand(1,9999);
        $search = 4;
        echo 'We can catch ' . $search . ' in number ' . $whatNum . ' exactly ' . substr_count($whatNum,$search) . ' time' . '.';
        ?>
    </li>
</ul>
<h4>Task 5</h4>
<p>Переменные:</p>
<ul>
    <li><?php
        $a = 3;
        echo $a
        ?>
    </li>
    <br>
    <li><?php
        $a = 10;
        $b = 2;
        echo $a + $b . '</br>';
        echo $a - $b . '</br>';
        echo $a * $b . '</br>';
        echo $a / $b . '</br>';
        ?>
    </li>
    <br>
    <li><?php
        $c = 15;
        $d = 2;
        echo $result = $c + $d . '</br>';
        ?>
    </li>
    <br>
    <li><?php
        $a = 10;
        $b = 2;
        $c = 5;
        echo $a + $b + $c;
        ?>
    </li>
    <br>
    <li><?php
        $a = 17;
        $b = 10;
        $d = 1;
        echo $d = ($c = $a - $b);
        ?>
    </li>
</ul>
<br>
<h4>Task 6</h4>
<p>Сложите переменные $c и $d</p>
<ul>
    <li>
        <?php
        $a = 45;
        $d = 15;
        echo $result = $a + $d;
        ?>
    </li>
    <br>
</ul>
<br>
<h4>Task 7</h4>
<p>Строки</p>
<ul>
    <li>
        <?php
        $textStart = 'Hello, World!';
        echo $textStart;
        ?>
    </li>
    <br>
    <li>
        <?php
        $text1 = 'Hello, ';
        $text2 = 'World!';
        echo $text1 . $text2;
        ?>
    </li>
    <br>
    <li>
        <?php
        $secInminute = 60;
        $minInhours = 60;
        $houInday = 24;
        $daysInweek = 7;
        $daysInmonth = 30;

        echo 'Seconds in hour ' . '= ' . $secInhour = $secInminute * $minInhours . '</br>';
        echo 'Seconds in day ' . '= ' . $secInday = $secInhour * $houInday . '</br>';
        echo 'Seconds in week ' . '= ' . $secInweek = $secInday * $daysInweek . '</br>';
        echo 'Seconds in month ' . '= ' . $secInmonth = $secInday * $daysInmonth . '</br>';
        ?>
    </li>
</ul>
<h4>Task 8</h4>
<p>Переделайте приведенный код</p>
<ul>
    <li>
        <?php
        $var = 1;
        $var += 12;
        $var -= 14;
        $var *= 5;
        $var /= 7;
        $var %= 1;
        echo $var;
        ?>
    </li>
    <br>
    <li>
</ul>
<h3>Second part "**"</h3>
<h4>Task 1</h4>
<p>Создайте три переменные - час, минута, секунда. </p>
<ul>
    <li>
        <?php
        $hour = 22;
        $minute = 34;
        $second = 56;
        echo $hour . ':' . $minute . ':' . $second;
        //        $timeNow= date("h:i:s");
        //        echo $timeNow;
        ?>
    </li>
</ul>
<h4>Task 2</h4>
<p>Переделайте этот код так, чтобы в нем использовалась операция .=.</p>
<ul>
    <li>
        <?php
        $text .= 'Я';
        $text .= ' хочу';
        $text .= ' знать';
        $text .= ' PHP!';
        echo $text;
        ?>
    </li>
</ul>
<h4>Task 3</h4>
<p>Задана переменная $foo = 'bar';</p>
<ul>
    <li>
        <?php
        $foo = 'bar';
        $bar = 10;
        $foo = $bar;
        echo $foo
        ?>
    </li>
</ul>
<h4>Task 4</h4>
<p>Какой будет результат если:</p>
<ul>
    <li>
        <?php
        $a = 2;
        $b = 4;
        // res = 6
        echo $a++ + $b;
        // res = 8
        echo $a + ++$b;
        // res = 9
        echo ++$a + $b++;
        ?>
    </li>
</ul>
<h4>Task 5</h4>
<p>Функции для проверки типов данных. </p>
<ul>
    <li>
        <?php
        $a = 'Hi';
        echo isset($a) ? 'variable exist' : 'variable not exist' ; ?>
    </li>
    <li>
        <?php
        $a = 'Hi';
        echo gettype($a) ? 'it is string' : 'it is not a string' ; ?>
    </li>
    <li>
        <?php
        $a = null;
        echo is_null($a) ? 'this variable is null' : 'this variable is not null' ; ?>
    </li>
    <li>
        <?php
        $a = '';
        echo empty($a) ? 'this is empty' : 'this is not empty' ; ?>
    </li>
    <li>
        <?php
        $a = '45';
        echo is_integer($a) ? 'this is integer' : 'this is not integer' ; ?>
    </li>
    <li>
        <?php
        $a = 3.14;
        echo is_double($a) ? 'variable is double' : 'variable is not double' ; ?>
    </li>
    <li>
        <?php
        $a = 'PHP is cool!';
        echo is_string($a) ? 'this is a string' : 'this is not a string' ; ?>
    </li>
    <li>
        <?php
        $a = 'Hallo!';
        echo is_numeric($a) ? 'this is a numeric' : 'this is not a numeric' ; ?>
    </li>
    <li>
        <?php
        $a = true;
        echo is_bool($a) ? 'this is a bool' : 'this is not a bool' ; ?>
    </li>
    <li>
        <?php
        $a = 59;
        echo is_scalar($a) ? 'this is a scalar' : 'this is not a scalar' ; ?>
    </li>
    <li>
        <?php
        $a = ['Gogo', 'Hoho', 'Dodo'];
        echo is_array($a) ? 'this is a array' : 'this is not a array' ; ?>
    </li>
    <li>
        <?php
        $a = (object) array('Gogo', 'Hoho', 'Dodo');
        echo is_object($a) ? 'this is a object' : 'this is not a object' ; ?>
    </li>
</ul>
<h3>Third part "***"</h3>
<h4>Task 1</h4>
<p>Даны два числа. Найти их сумму и произведение. </p>
<ul>
    <li>
        <?php
        $numFirst = rand(0,10);
        $numSecond = rand(0,10);
        $numThird = rand(0,10);
        echo
            '1st: ' . $numFirst,
            '<br> 2nd: ' . $numSecond,
            '<br> 3rd: ' . $numThird,
            '<br> сумма numFirst + numSecond = ' . ($numFirst + $numSecond), '<br> множение numFirst * numSecond = ' . ($numFirst*$numSecond);
        ?>
    </li>
    <h4>Task 2</h4>
    <p>Даны два числа. Найдите сумму их квадратов.</p>
    <li>
        <?php
        echo 'summ: ' . $summThird = pow($numFirst,2) + pow($numSecond,2);
        ?>
    </li>
    <h4>Task 3</h4>
    <p>Даны три числа. Найдите их среднее арифметическое.</p>
    <li>
        <?php
        echo 'summ: ' . $summSecond = ($numFirst + $numSecond + $numThird)/3;
        ?>
    </li>
    <h4>Task 4</h4>
    <p>Даны три числа x,y и z. Найдите (x+1)−2(z−2x+y)</p>
    <li>
        <?php
        $x = 3;
        $y = 2;
        $z = 4;
        echo $summForth = ($x + 1) - 2 * ($z - 2 * $x + $y);
        ?>
    </li>
    <h4>Task 5</h4>
    <p>Дано натуральное число...</p>
    <li>
        <?php
        $numNat = rand(1,15);
        echo $numNat;
        echo '<br>';
        echo 'остатки от деления на 3: ' . $numNat % 3;
        echo '<br>';
        echo 'остатки от деления на 5: ' . $numNat % 5;
        echo '<br>';
        $numNew = 25;
        $numNew += 0.3 * $numNew;
        echo $numNew;
        echo '<br>';
        $numNew += 1.2 * $numNew;
        echo $numNew;


        ?>
    </li>
    <h4>Task 6</h4>
    <p>Дано два числа...</p>
    <li>
        <?php
        $cc = 38;
        $bb = 10;
        $sum = 0.4 * $cc + 0.84 * $bb;
        echo $sum;
        echo '<br>';
        $new_num = 325;
        $dd = (string)$new_num;
        echo $dd[2] + $dd[1] + $dd[0];
        ?>
    </li>
    <h4>Task 7</h4>
    <p>Дано трехзначное числа. </p>
    <li>
        <?php
        $f = 476;
        $f = (string)$f;
        $f[1] = 0;
        echo $f[2] . $f[1] . $f[0];
        ?>
    </li>
    <h4>Task 8</h4>
    <p>Задача деление по модулю.</p>
    <li>
        <?php
        $firstN = 10;
        $secondN = 11;
        echo $firstN % 2 == 0 ? 'это четное' : 'это не четное';
        echo '<br>';
        echo $secondN % 2 == 0 ? 'это четное' : 'это не четное';
        ?>
    </li>
</ul>
</body>
</html>-->
-->
