<?php

$userName = 'Danylo';
$userAge = '21';
$pi = 3.14;

$firstArray = [‘alex’, ‘vova’, ‘tolya’];
$secondArray = [‘alex’, ‘vova’, ‘tolya’, [‘kostya’, ‘olya’]] ;
$thirdArray = [‘alex’, ‘vova’, ‘tolya’, [‘kostya’, ‘olya’, [‘gosha’, mila]]];
$fourthArray = [[‘alex’, ‘vova’, ‘tolya’], [‘kostya’, ‘olya’], [‘gosha’, mila]];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h4>User name:</h4>
<ul>
    <li><?php echo $userName?></li>
</ul>
<h4>User age:</h4>
<ul>
    <li><?php echo $userAge?></li>
</ul>
<h4>Pi N</h4>
<ul>
    <li><?php $pi?></li>
</ul>
<h5>Other users:</h5>
<ul>
    <li>
        <pre><?php print_r($firstArray)?></pre>
    </li>
    <li>
        <pre><?php print_r($secondArray)?></pre>
    </li>
    <li>
        <pre><?php print_r($thirdArray)?></pre>
    </li>
    <li>
        <pre><?php print_r($fourthArray)?></pre>
    </li>
</ul>
</body>
</html>


